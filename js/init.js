var config = {
    type: Phaser.WEBGL,
    width: 800,
    height: 600,
    backgroundColor: 'black', 
    physics:{
        default: "arcade",
        arcade: {
            fps: 60,
            gravity: { x: 0, y: 0 }
        }
    },
    scene: [mainMenu, PlayScene, GameOver],
    pixelArt: true,
    roundPixels: true
}
const game = new Phaser.Game(config);
