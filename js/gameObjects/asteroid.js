     class Asteroid extends Phaser.GameObjects.Sprite {

         constructor(scene, x, y, nombre, velocidad, xo, yo) {

             super(scene, x, y, nombre);

             this.speed = Phaser.Math.GetSpeed(velocidad, Math.random() * (1 - 0.7) + 0.7);
             this.direction = 0;
             this.angle = 0;
             this.orbiting = false;
             this.factor = 0.5;
             this.xOrigin = Phaser.Math.RND.between(0, 800);
             this.yOrigin = 0;
             if (!xo || !yo) {
                 var x = Math.floor(Math.random() + 0.5);
                 if (x == 1) {
                     this.yOrigin = Phaser.Math.RND.between(550, 600);
                 } else {
                     this.yOrigin = Phaser.Math.RND.between(0, 50);
                 };
                 this.setPosition(this.xOrigin, this.yOrigin);
             } else {
                 this.yOrigin = yo;
                 this.xOrigin = xo;
                 this.factor = Math.random();
                 this.angle = Math.random();
                 this.setPosition(this.xOrigin, this.yOrigin);
             }

         }

         isOrbiting() {
             return this.orbiting;
         }

         fire(shipX, shipY) {
             this.setSize(32, 29);
             this.orbiting = true;

             this.setActive(true);
             this.setVisible(true);



             if (shipX > this.xOrigin) {
                 let m = (shipY - this.yOrigin) / (shipX - this.xOrigin);
                 this.direction = Math.atan(m);
                 if (this.texture.key == 'asteroid-2') {
                     this.direction = Math.atan(m) * Math.random()
                 }
             } else {
                 this.factor = -1;
                 let m = (shipY - this.yOrigin) / (this.xOrigin - shipX);
                 this.direction = Math.atan(m);
                 if (this.texture.key == 'asteroid-2') {
                     this.direction = Math.atan(m) * (Math.random() + 1)
                 }
             }

             this.angleRotation = Phaser.Math.RND.between(0.2, 0.9);
         }

         update(time, delta) {
             if (this.texture.key == 'asteroid-2') {
                 this.x += this.factor * Math.cos(this.direction) * this.speed * delta;
                 this.y += Math.sin(this.direction) * this.speed * delta;
             } else {
                 this.x += this.factor * Math.cos(this.direction) * this.speed * delta;
                 this.y += Math.sin(this.direction) * this.speed * delta;
             }

             this.angle += this.angleRotation;

             if (this.x < -50 || this.y < -50 || this.x > 800 || this.y > 600) {
                 this.setActive(false);
                 this.setVisible(false);
             }

         }
     }