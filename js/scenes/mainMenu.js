class mainMenu extends Phaser.Scene {
    constructor() {
        super({
            key: 'mainMenu'
        });
    }

    preload() {

        //boton play y sus sonidos
        this.load.image('sprBtnPlay', 'img/play.png');
        this.load.image('sprBtnPlayHover', 'img/play2.png');
        this.load.image('sprBtnPlayDown', 'img/play.png');
        this.load.audio('sndBtnOver', 'recursos/Sound/Gold3.wav');
        this.load.audio('sndBtnDown', 'recursos/Sound/Gold4.wav');

        //musica de fondo
        this.load.audio('musicaMenu', 'recursos/Music/2.ogg');
        this.load.audio('musicaJuego', 'recursos/Music/3.ogg');
        this.load.audio('musicaGameOver', 'recursos/Music/4.ogg');

        //vidas
        this.load.image('vidaLlena', 'recursos/HUD/FullLife.png');
        this.load.image('vidaVacia', 'recursos/HUD/EmptyLife.png');
        this.load.audio('ggVida', 'recursos/Sound/6.wav' )


        //fondo
        this.load.image('background', 'recursos/Background/3b.png');

        //imag de gameover
        this.load.image('gameOver', 'img/Game-Over.png');
        this.load.image('gameOverBG', 'img/GameOverFondo.png');
        this.load.image('restart1', 'img/restart1.png');
        this.load.image('restart2', 'img/restart2.png')

        //explosion
        this.load.spritesheet('sprExplosion', 'recursos/Fx/Fx2.png', {
            frameWidth: 34,
            frameHeight: 34
        });

        //asteroides
        this.load.image('asteroid-1', 'img/asteroid-1.png');
        this.load.image('asteroid-2', 'img/asteroid-2.png');
        this.load.image('asteroid-3', 'img/asteroid-3.png');
        this.load.image('asteroid-4', 'img/asteroid-4.png');
        this.load.audio('radarDelDragon', 'recursos/Sound/alert.wav')

        //laser y nave
        this.load.image('shoot', 'recursos/Shoot/4.png');
        this.load.image('ship', 'recursos/Ship/jelly-2.png');
        this.load.audio('sndExplode0', 'recursos/Sound/darkShoot.wav');
        this.load.audio('sndExplode1', 'recursos/Sound/1.wav');
        this.load.audio('sndLaser', 'recursos/Sound/Laser.wav');

        //muerte
        this.load.audio('deathSound', 'recursos/Sound/Death.wav')


    }

    create() {
        this.sfx = {
            btnOver: this.sound.add('sndBtnOver'),
            btnDown: this.sound.add('sndBtnDown')
        };

        var music = this.sound.add('musicaMenu');
        music.setLoop(true);
        music.play();
        music.setVolume(0.3);

        this.btnPlay = this.add.sprite(
            this.game.config.width * 0.5,
            this.game.config.height * 0.5,
            'sprBtnPlay'
        );

        this.btnPlay.setInteractive();

        this.btnPlay.on(
            'pointerover',
            function () {
                this.btnPlay.setTexture('sprBtnPlayHover'); 
                this.sfx.btnOver.play();
            },
            this
        );

        this.btnPlay.on('pointerout', function () {
            this.setTexture('sprBtnPlay');
        });

        this.btnPlay.on(
            'pointerdown',
            function () {
                this.btnPlay.setTexture('sprBtnPlayDown');
                this.sfx.btnDown.play();
            },
            this
        );

        this.btnPlay.on(
            'pointerup',
            function () {
                this.btnPlay.setTexture('sprBtnPlay');
                music.stop();
                this.scene.start('PlayScene');
            },
            this
        );

        this.title = this.add.text(
            this.game.config.width * 0.5,
            128,
            'ASTEROIDS', {
                fontFamily: 'monospace',
                fontSize: 48,
                fontStyle: 'bold',
                color: '#ffffff',
                align: 'center'
            }
        );

        this.title.setOrigin(0.5);

    }

    update() {}
}