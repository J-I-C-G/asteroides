class PlayScene extends Phaser.Scene {


    constructor() {
        super({
            key: 'PlayScene'
        });
        this.lastFired = 0;
        this.asteroidElapsedTime = 3000;
        this.gameOver = false;
        this.score = 0;
        this.scoreText = "PATO";

    }

    preload() {}

    create() {

        //musica y sonidos
        this.music = this.sound.add('musicaJuego');
        this.music.setLoop(true);
        this.music.play();
        this.music.setVolume(0.1);

        this.sfx = {
            shootSound: this.sound.add('sndExplode1'),
            explosionSound: this.sound.add('sndExplode0'),
            deathSound: this.sound.add('deathSound'),
            ggVidaSound: this.sound.add('ggVida'),
            radarSound: this.sound.add('radarDelDragon')
        }


        //fondo
        this.add.image(0, 0, 'background');
        this.add.image(640, 0, 'background');
        this.add.image(0, 480, 'background');
        this.add.image(640, 480, 'background');

        //vidas atr
        this.vidas = 3;
        this.imgvidas = {
            vida1: this.add.image(710, 30, 'vidaLlena'),
            vida2: this.add.image(740, 30, 'vidaLlena'),
            vida3: this.add.image(770, 30, 'vidaLlena')
        }

        //fisicas de la nave
        this.ship = this.physics.add.image(400, 300, 'ship');
        this.ship.setDamping(true);
        this.ship.setDrag(0.99);
        this.ship.setMaxVelocity(300);
        this.ship.setCollideWorldBounds(true);
        this.ship.setSize(20, 30);

        //animación de explosión
        this.anims.create({
            key: 'sprExplosion',
            frames: this.anims.generateFrameNumbers('sprExplosion'),
            frameRate: 20,
            repeat: 0
        });

        //crea los input del teclado
        this.cursors = this.input.keyboard.createCursorKeys();

        // fisica de los disparos
        this.shootsGroup = this.physics.add.group({
            classType: Shoot,
            maxSize: 10,
            runChildUpdate: true
        });

        //crea el grupo de asteroides y le asigna tiempo de respawn
        this.asteroidsGroup = this.physics.add.group();
        this.asteroidsArray = [];
        this.asteroidsTimedEvent = this.time.addEvent({

            delay: this.asteroidElapsedTime,
            callback: this.addAsteroid,
            callbackScope: this,
            loop: true,
        });

        // score text
        this.scoreText = this.add.text(16, 16, '0', {
            font: '48px Times',
            fill: 'white'
        });

        // agrega el overlap y collider
        this.physics.add.overlap(this.ship, this.asteroidsGroup, this.hitShip, null, this);
        this.physics.add.collider(this.shootsGroup, this.asteroidsGroup, this.hitShoot, null, this);
        this.gameOver = false;





    }

    update(time, delta) {
        //variables globales plox
        this.p = Math.floor(Math.random() * 2);
        this.asd = 30;

        if (this.gameOver) {
            return;
        }

        //cursores
        if (this.cursors.up.isDown) {
            this.physics.velocityFromRotation(this.ship.rotation, 200, this.ship.body.acceleration);
        } else {
            this.ship.setAcceleration(0);
        }

        if (this.cursors.space.isDown && time > this.lastFired) {
            let shoot = this.shootsGroup.get();

            if (shoot) {
                shoot.fire(this.ship.x, this.ship.y, this.ship.rotation);
                this.sfx.shootSound.play();
                this.lastFired = time + 50;
            }
        }

        if (this.cursors.left.isDown) {
            this.ship.setAngularVelocity(-300);
        } else if (this.cursors.right.isDown) {
            this.ship.setAngularVelocity(300);
        } else {
            this.ship.setAngularVelocity(0);
        }

        //update de los asteroides (?)
        this.asteroidsArray = this.asteroidsArray.filter(function (asteroid) {
            return asteroid.active;
        });

        for (let asteroid of this.asteroidsArray) {
            if (!asteroid.isOrbiting()) {
                asteroid.fire(this.ship.x, this.ship.y);
            }

            asteroid.update(time, delta);
        }
    }


    addAsteroid() {
        var xo, yo;
        switch (this.p) {
            case 0:
                this.nombre = 'asteroid-1'
                this.velocidad = 100
                break;
            case 1:
                this.nombre = 'asteroid-3'
                this.velocidad = 200
                break;
            case 2:
                this.nombre = 'asteroid-2'
                this.velocidad = 200
                xo = this.xo
                yo = this.yo
                break;
        }
        let asteroid = new Asteroid(this, 0, 0, this.nombre, this.velocidad, xo, yo);
        this.asteroidsGroup.add(asteroid, true);
        this.asteroidsArray.push(asteroid);
        this.sfx.radarSound.play();

    }


    hitShip(ship, asteroid) {
        if (this.vidas == 1) {

            this.imgvidas.vida3.setTexture('vidaVacia');
            this.sfx.ggVidaSound.play();
            this.sfx.deathSound.play();
            this.physics.pause();
            this.asteroidsTimedEvent.paused = true;

            this.ship.setTint(0xff0000);
            this.ship.body.allowRotation = false;

            this.gameOver = true;
            ship.destroy();
            this.time.addEvent({

                // play gameover
                delay: 1000,
                callback: function () {
                    this.scene.start('GameOver');
                },
                callbackScope: this,
                loop: false
            });
            this.music.stop();
            asteroid.destroy();
            this.score = 0;
        } else {
            this.vidas -= 1;
            asteroid.destroy();
            switch (this.vidas) {
                case 2:
                    this.imgvidas.vida1.setTexture('vidaVacia');
                    break;
                case 1:
                    this.imgvidas.vida2.setTexture('vidaVacia');
                    break;
            }
            this.sfx.ggVidaSound.play();

        }
    }

    hitShoot(shoot, asteroid) {
        this.sfx.explosionSound.play();
        asteroid.destroy();
        if (asteroid.texture.key == 'asteroid-1') {
            this.p = 2
            this.xo = asteroid.x
            this.yo = asteroid.y
            this.addAsteroid()
            this.addAsteroid()
            this.addAsteroid()

        }

        switch (asteroid.texture.key) {
            case 'asteroid-1':
                this.score += 5;
            case 'asteroid-2':
                this.score += 10;
            case 'asteroid-3':
                this.score += 7;

        }
        this.scoreText.setText(this.score);
    }
}