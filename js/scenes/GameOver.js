class GameOver extends Phaser.Scene {


    constructor() {
        super({
            key: 'GameOver'
        });
    }

    preload() {}

    create() {
        this.music = this.sound.add('musicaGameOver');
        this.music.setLoop(true);
        this.music.play();
        this.music.setVolume(0.3);

        this.add.image(0, 0, 'gameOverBG');
        this.add.image(640, 0, 'gameOverBG');
        this.add.image(0, 480, 'gameOverBG');
        this.add.image(640, 480, 'gameOverBG');
        this.add.image(400, 300, 'gameOver');
        this.add.image('restart1');
        this.add.image('restart2');


        this.sfx = {
            btnOver: this.sound.add('sndBtnOver'),
            btnDown: this.sound.add('sndBtnDown')
        };

        this.restart = this.add.sprite(
            this.game.config.width * 0.5,
            this.game.config.height * 0.85,
            'restart2'
        );
        this.restart.setInteractive();

        this.restart.on(
            'pointerup',
            function () {
                this.music.stop();
                this.restart.setTexture('restart1');
                this.scene.start('PlayScene');
            },
            this
        );


        this.restart.on(
            'pointerover',
            function () {
                this.restart.setTexture('restart1'); 
            },
            this
        );

        this.restart.on(
            'pointerout', 
            function () {
                this.restart.setTexture('restart2');
            },
            this
        );

        this.restart.on(
            'pointerdown',
            function () {
                this.restart.setTexture('restart2');
                this.sfx.btnDown.play();
            },
            this
        );

    }
    update() {}

}